﻿/******************************************************************************
 *             NavBallTextureChanger for Kerbal Space Program                 *
 *                                                                            *
 * Version 1.4                                                                *
 * Created: 2/14/2014 by xEvilReeperx                                         *
 * ************************************************************************** *
 
This is free and unencumbered software released into the public domain.

Anyone is free to copy, modify, publish, use, compile, sell, or
distribute this software, either in source code form or as a compiled
binary, for any purpose, commercial or non-commercial, and by any
means.

In jurisdictions that recognize copyright laws, the author or authors
of this software dedicate any and all copyright interest in the
software to the public domain. We make this dedication for the benefit
of the public at large and to the detriment of our heirs and
successors. We intend this dedication to be an overt act of
relinquishment in perpetuity of all present and future rights to this
software under copyright law.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
 * ***************************************************************************/
/* Changelog
 * 1.5 -
 *      - Updated for 1.2
 *      
 * 1.4 -
 *      - Updated for 1.1.2
 *      
 * 1.3 -
 *      - Updated for 0.24
 *      - Fixed an issue reported by someone that involves FindObjectOfType
 *      
 * 1.2 -
 *      Fixed issue (well, oversight) which caused vessels with multiple iva
 *      navballs to have only one changed
 * 
 * 1.1.a -
 *      - The current directory being elsewhere than KSP root no longer breaks the
 *        plugin
 *      - Folder name and path is now irrelevant; the plugin will simply look inside
 *        the DLL's directory for navball.png, regardless of the plugin's actual path
 *          note: still needs to be somewhere under GameData though
 *
 * 1.1 - 
 *      Fixed issue preventing NavBall texture from being changed if the vessel doesn't have an iva view
 * 
 * 1.0.a - 
 *      Now includes iva
 * 
 * 1.0 -
 *      Released 
*/

using System;
using System.IO;
using System.Linq;
using System.Reflection;
using KSP.IO;
using NavBallTextureChanger.Extensions;
using UnityEngine;
using File = System.IO.File;

// ReSharper disable FieldCanBeMadeReadOnly.Local
// ReSharper disable UnusedMember.Local

namespace NavBallTextureChanger
{
    [KSPAddon(KSPAddon.Startup.Flight, false)]
    public class NavBallChanger : MonoBehaviour
    {
        private const string ConfigFileName = "config.cfg";

        [Persistent] private NavBallTexture _navballTexture = new NavBallTexture(GetSkinDirectory());

        private void Awake()
        {
            LoadConfig();


            GameEvents.onVesselChange.Add(OnVesselChanged);
            GameEvents.OnCameraChange.Add(OnCameraChanged);

            _navballTexture.SaveCopyOfStockTexture();
            UpdateFlightTexture();
        }


        private void OnDestroy()
        {
            GameEvents.onVesselChange.Remove(OnVesselChanged);
            GameEvents.OnCameraChange.Remove(OnCameraChanged);
        }


        private void LoadConfig()
        {
            var configPath = IOUtils.GetFilePathFor(typeof(NavBallChanger), ConfigFileName);
            var haveConfig = File.Exists(configPath);

            if (!haveConfig)
            {
                Log.Message("Config file not found. Creating default");

                if (!Directory.Exists(Path.GetDirectoryName(configPath)))
                    Directory.CreateDirectory(Path.GetDirectoryName(configPath));

                ConfigNode.CreateConfigFromObject(this).Save(configPath);
            }

            configPath
                .With(ConfigNode.Load)
                .Do(config => ConfigNode.LoadObjectFromConfig(this, config));
        }


        private void OnVesselChanged(Vessel data)
        {
            _navballTexture.Do(nbt => nbt.MarkMaterialsChanged());
            UpdateFlightTexture();
        }


        // Reset textures when entering IVA. Parts might have loaded or changed their internal spaces
        // in the meantime
        private void OnCameraChanged(CameraManager.CameraMode mode)
        {
            if (mode != CameraManager.CameraMode.IVA) return;

            UpdateIvaTextures();
        }


        private void UpdateFlightTexture()
        {
            _navballTexture
                .Do(nbt => nbt.SetFlightTexture());
        }


        private void UpdateIvaTextures()
        {
            _navballTexture.SaveCopyOfIvaEmissiveTexture();
            _navballTexture.Do(nbt => nbt.SetIvaTextures());
        }


        private static UrlDir GetSkinDirectory()
        {
            var skinUrl = AssemblyLoader.loadedAssemblies.GetByAssembly(Assembly.GetExecutingAssembly()).url + "/Skins";

            var directory =
                GameDatabase.Instance.root.AllDirectories.FirstOrDefault(d => d.url == skinUrl);

            if (directory == null)
                throw new InvalidOperationException("Failed to find skin directory inside GameDatabase");

            return directory;
        }
    }
}
