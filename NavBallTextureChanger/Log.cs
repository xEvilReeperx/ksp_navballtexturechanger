﻿using UnityEngine;

namespace NavBallTextureChanger
{
    static class Log
    {
        private const string Prefix = "NavBallChanger: ";

        public static void Message(string message)
        {
            Debug.Log(Prefix + message);
        }

        public static void Error(string message)
        {
            Debug.LogError(Prefix + message);
        }
    }
}
